{
    serviceFrontend = mkResource SERVICE_FRONTEND;
    dnsProvider = provider: credentials: { inherit provider credentials; };
    httpTarget = frontend: domains: mkResource HTTP_TARGET { inherit frontend domains; };

    garageStore.make = borg: port: peers: mkResource GARAGE_STORE { inherit borg port peers; };
    garageStore.expand = provider: rec {
        buckets = context.findConsumers (consumer: consumer.store) GARAGE_BUCKET;
        secrets = map (bucket: bucket.key) buckets;
    };

    pgsqlServer.make = port: mkResource PGSQL_SERVER { inherit port; };
    sqlServer.expand = context: provider: {
        databases = findConsumers context MYSQL_DATABASE;
        secrets = map (database: database.password) databases;
    };

    borgServer = port: mkResource BORG_SERVER { inherit port; } {
        cluster = findConsumers context GARAGE_STORE;
    };

    persistentVolume = pool: path: mkResource PERSISTENT_VOLUME { inherit pool path; };
    storeVolume = store: pool: path: mkResource STORE_VOLUME { inherit store pool path; };
    storeBucket = store: name: mkResource STORE_BUCKET { inherit store name; };

    pgsqlDatabase = server: store: name: mkResource MYSQL_DATABASE { inherit server store name; };

}
