# Secret management in Canix

As secrets are usually a property of an interaction between nodes rather than something that is only needed in a single node, in Canix they are typically defined in resource descriptions (but not always).

keywords

 - we want secrets to be loaded in secure memory and passed to the software that needs them without them ever being stored to disk (using the systemd credential tooling)
 - very often, a secrets needs to be in at least two places (configured at a client and at a server), or two secrets will have a specific relation (public/private keys, etc)
 - we need to know how to generate secrets beforehand
   - their actual value is not part of the specification (it only specifies how to create them)
   - the deployment tooling needs to be able to feed them, making sure they are only visible where they are needed
   - a service should not be able to have access to any secrets other than the one referenced in its specification

 - only persist required user data, no implementation data, except in the nix store
 - nix store is world readable and should be seen as public
 - 
 - we want to avoid having the secrets persisted on the physical machines
 - so they need to be loaded from elsewhere, and loaded