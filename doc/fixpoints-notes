
# Additional notes
# ----------------------------------------------------------------------------
#
# This part of the code may feel a bit like magic. We define functions 
# that take their own input as arguments, but how are we supposed to 
# call them then ? How can that even work ? 
#
# `magic = output: output // { y = 1; x = output.y * 2; };`
#
# If you squint at that hard enough, it may remind of the `rec` keyword in Nix, 
# and indeed, what we are doing here is a recursive binding. This is also something
# that happens in `let` bindings, where each definition is allowed to refer to others.
#
# Well, it turns out that we can evaluate the output of these functions,
# because Nix not only lets us refer to other definitions in a let-binding,
# but also allows us to refer to a binding in its own definition :
#
# `let x = magic x; in x;`
#
# Here, we are instructing nix that "magic(x) = x", and asking the interpreter 
# to find a value of `x` where this equation holds true. This value of `x` is called
# the fix-point of `magic` : for this value, `magic` returns exactly the same value it
# was given, breaking the recursion.
#
# Of course, this can work only where the recursion this creates is terminating, which 
# is akin to saying "only when the function has a fixed-point". Even in those cases, the
# evaluator will not always be able to determine the fixed-point, for example, if we define
# `zero = output: output / 2`, Nix will not be able to determine that the fixed-point of this
# function is actually 0. However, since attribute members are evaluated lazily (remember that
#  `x.y` is just `getAttr x y` and that function evaluation in Nix is lazy), Nix can find the 
# fixed-point of such recursive bindings, as long as there are no cycles in the definitions.
#
# In NixOS, this pattern is used a lot, it is what allows us to refer to `config` when writing 
# NixOS configuration modules.
#
# Finally, in order to make interacting with such functions a bit easier, Nix defines this
# function combinator : it takes a function, and returns its fixed-point.
#
# `fix = f: x: let x = f x; in x;`