# Networking setup in Canix

## Tunneling

In infrastructures defined by Canix, some service hosts may be located on external networks,
and Canix allows to set up backbone Wireguard tunnels for such hosts. These are point-to-point
tunnels that connect externally-hosted service hosts to "exit points", from where they can reach
the rest of the internal network as well as the wider Internet.

## Addressing

In infrastructures defined by Canix, service hosts typically have their own public IPv4 and
IPv6 address, that are routed to them through the Wireguard tunnel. Services themselves are
typically not connected directly to outer networks : their traffic is proxied through L7
gateways, such as a forward HTTP proxy (ex: squid), and a reverse HTTP proxy (ex: haproxy).
This separation is necessary in IPv4 because HTTPS traffic is routed to services based on the
Host header (or the SNI for HTTPS), and is useful for both IPv4 and IPv6 because we can apply
some filtering (both for ingress or egress traffic) and configure some limits at the
protocol-level. It also means we don't need to do any kind of NAT/masquerading. However, it is
still possible to use NAT for some specific containers, when L7 proxying is not adapted
possible, or for containers that actually require unrestricted outbound traffic.

In later improvements to Canix, we can envision to have direct IPv6 access for some services, 
however for the first versions it is out-of-scope.

## Routing

In infrastructure defined by Canix, /32 IP addresses are assigned to each host, and
interface-level routes are added for each of the host's peer, in a typical point-to-point
link configuration. However, while link addresses can be configured statically, since
some hosts may use redundant tunnels or may use redundant exit points, the precise way the
traffic is routed can not always be configured statically. In order to implement these
redundancy schemes, we use OSPF to redistribute routes in our internal network. This also
simplifies the networking code in Canix, as it means we don't have to infer the networks that
should be routed to/from a given host.

In later improvements to Canix, we can envision using a distinct zone for each Canix-based
deployment, so that multiple deployments can be federated using the backbone zone. It could 
also be possible to switch to another dynamic routing protocol to avoid having to manage zones
and have summarization available at every node in the routing graph, but right now, OSPF is 
the protocol that Canix's author knows best, so for the first versions we will use OSPF.

## Service networks

Even if services are meant to be isolated from external networks (and from some parts of the
internal network), they still need to be able to communicate with their parent host, in some
cases with other services, and as said in previous sections, some of them actually need IP 
communication (through NAT and masquerading), so we still need to set up an IP stack for
services. In typical docker/podman deployments, this is handled through user-mode networking
(with tools like `slirp4netns` or `pasta`), however this is quite limiting (it increases load
on the host when the traffic is high, only allows TCP/UDP, no ICMP or other types, and requires
routing traffic to services through ports on the service host). With Canix, we still want the 
service containers to be "rootless" (they should start from an unprivileged context), so we
preemptively set up a dedicated network namespace in the service host, we connect this
namespace with the root namespace using a dedicated virtual ethernet pair, and assign a
"service router" address to its downstream side. This means that the service hypervisor (which
will run in this dedicated network namespace) will be able set up other virtual ethernet pairs
for each service, and route them through the virtual device connecting it to the root namespace.

```
   +---------------------------------------------------------------+
   | +-----------------------------------------------------------+ |
   | | +----------------+ +----------------+  +----------------+ | |
   | | |   service 2    | |   service 2    |  |   service 1    | | |
   | | | 10.13.208.1/32 | | 10.13.208.1/32 |  | 10.13.208.1/32 | | |
   | | |       /\       | |       /\       |  |       /\       | | |
   | | +-------||-------+ +-------||-------+  +-------||-------+ | |
   | |         \/ veth            \/ veth             \/ veth    | |
   | |                                                           | |
   | | +------------+  service routing namespace                 | |
   | | |  Rootless  |        10.13.208.255/32                    | |
   | | | hypervisor |                                            | |
   | | +------------+             /\                             | |
   | +----------------------------||-----------------------------+ |
   |                              \/ veth                          |
   |                                                               |
   | +------------+   service host root namespace   +------------+ |
   | |  Forward   |          10.13.255.208/32       |   Reverse  | |
   | | HTTP proxy |             1.2.3.4/32          | HTTP proxy | |
   | +------------+                                 +------------+ |
   +---------------------------------------------------------------+
   Fig 1. : layered network namespace structure for service hosts
```

Note that this setup can also be used to implement production/preproduction environments.