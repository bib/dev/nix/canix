# Draft notes for the inner system interface in Canix

Right now, one of the main aspects of Canix that is not well-defined is 
the "inner system interface".

This is the part of the specification that describes the inner system
(operating system, or application container) that should run in a node.
This interface will be used both for machine nodes, and for service nodes.

Service nodes may be implemented using containarization or virtualization.

This interface would be relatively simple if we restricted inner systems to
NixOS, but one of the objectives was to keep the ability to run Debian-based
service nodes. Debian-based machine nodes could also be theoretically supported,
however resource types would have to be implemented for Debian as wll.

If we want to allow Debian-based systems, we need to have some form of
declarative interface that allows us to pregenerate the container image 
(or to apply changes to an existing base image). However, since generating 
these container images (or applying the configuration to a base image) would
typically be quite slow, we also want to allow some nodes to persist their
operating system data, and believe in the OS declarative interface's ability
to preserve idempotency and reproducibility.

There is also the question of where should this system interface be defined
in Canix. Recently, we thought of the possibility of system interfaces to
be defined as resources, which would make a lot of sense, but the end-result
is still a bit blurry right now.