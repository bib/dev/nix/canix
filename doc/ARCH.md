# Canix architecture

## Principle of operation

In Canix, the infrastructure is specified as **machines** and as **services**, that
can be instantiated on machines, so that the same service on multiple machines doesn't
have to be described multiple times. Each node in this infrastructure describes
its inner configuration as well as resources exposed to other nodes and used
when evaluating the inner configuration.

Resources are used to describe the interfaces between the different nodes of the graph. For example, a service that configures a MySQL server can use a MYSQL_SERVER resource to say that it provides a MySQL server. Other services can then use a MYSQL_DATABASE resource targeted at the MYSQL_SERVER resource, so that their configuration can depend on the actual server's address, port, and other details. Then, when resources are expanded, they are made aware of other resources that reference them, so that the MYSQL_SERVER resource can handle database creation and setting up accounts for each client.

Resources are defined **outside** of the node's inner configuration : they can be fully evaluated and expanded without ever evaluating the inner configurations, which means they can be cached, as they won't be invalidated when the inner configurations change. It also means that the inner configurations can be specified independently of the rest of the infrastructure. Resources should be seen as a way to describe the way each node and service relate to each other, and to represent the functional dependencies between them. They can then be used by each node and service to actually implement what is described by the resource. It is a way to keep a loose coupling between the infrastructure's specification and the node's inner configurations.

## Implementation

**NOTE: This is completely outdated sincw we've rewritten everything with the NixOS module system**

This raw specification is built from two files (`machines.nix` and `services.nix`),
each time written as a function that take a `resources` argument containing
helper functions, and that returns an attribute tree containing the raw
specification. 

Canix will then process this specification, by first mapping services to their
target machines, and then, in sequential order :
 - annotating the graph nodes with contextual information (path, parent, name,
   etc) (see `stages/annotate.nix`)
 - resolving the paths in the node's attributes (using the annotations made in
   the previous stage) (see `stages/resolve.nix`)
 - expanding the resource definitions by instantiating them for each
   forward/back-reference (see `stages/resolve.nix`)

After all these stages, the expanded specification will consist of a forest of
machines, that each contain their instantiated services. Each node (machines/services)
will also contain its expanded resources, as well as the path to the
configuration modules that need to be evaluted to build the node's inner
configuration (the configuration of the system running in the container/VM for
this node). Like the raw specification, the expanded specification consists of
attrset trees, where values can be only be other attrsets, strings, booleans,
integers, floats and store-paths, so that it can easily be exported to JSON and
inspected in external tools. 

The expanded specification is evaluated without evaluating the nodes inner
configuration so that we can have some hermeticity between them and evaluate
them separately : both the expanded specification and each inner configuration
is stored in its own flake output ; their evaluation can each be cached
separately (see `default.nix`).

In order to implement all this, Canix uses a soft-typing scheme (see
`types.nix`), where some values are represented wrapped in an attrset that
describe their type. This is useful to represent things like resource
themselves, but also resource paths, node paths, secrets, etc, so that these
values can have particular meaning in specific stages of the specification
processing (for example, this allows path values to be resolved/expanded in the
second stage, or secret definitions to be gathered to define tools to set them
up).

Some other auxiliary code used to make this work is the path-handling code (see
`path.nix`), that implements logic to resolve relative paths (and expand globbed
paths), and the graph-handling code (see `graph.nix`), that implements logic to
navigate through graphs and find resource references and back-references.

The rest of the code is in the resource type definitions (see `resources/*/*`) :
they are what implement and define Canix's features (the rest is mainly a
scaffolding to make it work). Resource types define the helper definition
method, the expansion method, and provide OS configuration modules that
implement the resources in the node's inner configuration. 

