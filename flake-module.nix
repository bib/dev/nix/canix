{ lib, self, config, ... }: 

with lib; {
    
  options = {

    canix = mkOption {
      description = ''
      Canix infrastructure specification.

      This namespace provides the options used to define a Canix
      infrastructure specification. Its configuration is then used
      to derive the final NixOS configurations and deployment tools.
      '';
      type = types.submodule ./modules;
      default = {};
    };

    };

    config = {

    nixosConfigurations = mkMerge (canix.collectNixosSystems config.canix);
    # TODO: add deployment tools and infrastructure devshell
    
    };

  }