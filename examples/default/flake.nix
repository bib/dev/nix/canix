{
  description = "example.org infrastructure flake";

  inputs = {
    # Add the path to the Canix library flake
    #canix.url = "git+https://framagit:bib/dev/nix/canix/?ref=nixos-module-system";
    canix.url = "/home/user/data/bib/rps/dev/nix/canix";
    # Use NixOS 24.05 by default. 
    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.05";
    # Also require NixOS 23.05, as it is required by some services.
    nixpkgs_2305.url = "github:nixos/nixpkgs/nixos-23.05";
  };

  outputs = {canix, ...}: {
    # Evaluate the Canix specification
    canixSpecification = canix.lib.evalSpec ./configuration.nix;
  };

}
