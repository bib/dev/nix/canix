{ lib, ... }:
with lib; {

  config.machines = {

    # In our example infrastructure, adelia is a gateway machine, used to provide
    # routable IP addresses to our service machines (`makari` and `kayou`).
    #
    # It is only used as a Wireguard tunnel machine.
    adelia = rec {
      qualifiedName = "gateway.example.org";

      externalAddresses = [ "10.11.12.13" ];
      internalAddress = "10.0.0.255";

      resources.gateway.tunnelServer = {
        port = 1312;
        addresses = externalAddresses;
      };

      system.nixos.config = ./machines/adelia;
      platform = "x86-64_linux";
    };

    # In our example infrastructure, makari is a service machine, that is
    # "self-hosted", behind a public ISP connection, and by itself does
    # not have its own routable IP address. We specify that makari will
    # get a routable IP address through `adelia`, and set some rate-limiting
    # to avoid saturating the uplink connection.
    makari = rec {
      # Set the machine's qualified name. This name should resolve to the machine's
      # public address.
      qualifiedName = "makari.example.org";

      # Set the machine's network addresses. The service network will be used to
      # machine the service nodes. This machine has two public IP addresses, one IPv4
      # and one IPv6.
      externalAddresses = [ "1.2.3.4" "2024:0000:1312::1" ];
      internalAddress = "10.0.0.1";
      serviceNetwork = "10.0.1.0/24";

      # Define two storage pools, that services can use to store persistent data.
      resources.hot.storagePool = {
        mountpoint = "/srv/hot";
      };
      resources.cold.storagePool = {
        mountpoint = "/srv/cold";
      };

      # Specify our uplink tunnel through adelia, making it route our routable 
      # IP address to us.
      resources.uplink.tunnelClient = {
        # Connect to the `gateway` resource on `adelia`
        server = "/adelia:gateway";
        # Re-use the machine's external address for the tunnel address
        addresses = externalAddresses;
        # Set some rate-limiting
        limits.up = {
          rate = "200mbit";
          burst = "10mbit";
          limit = "500kbit";
        };
        limits.down = {
          rate = "100mbit";
          burst = "10mbit";
          limit = "500kbit";
        };
        # Indicate that we want all outgoing traffic to be routed through adelia
        requires = [ "0/0" "::/0" ];
        # Indicate that other machines connected to adelia may reach our service network.
        provides = [ serviceNetwork ];
      };

      # Specify a reverse proxy
      resources.proxy.reverseProxy = {
        # Make the reverse proxy listen both on the internal and public addresses
        addresses = externalAddresses ++ [ internalAddress ];
      };

      # Specify the inner system configuration
      system.nixos.config = ./machines/makari;
      platform = "x86-64_linux";
    };

    # In our example infrastructure, kayou is another service machine, but that
    # is colocated in a DC, and has its own routable IP address. We still need
    # a tunnel to connect to the rest of the infrastructure, but we don't need to
    # route all of our traffic through this tunnel.
    kayou = rec {

      # Set the machine's qualified name. This name should resolve to the machine's
      # public address.
      qualifiedName = "kayou.example.org";

      # Set the machine's network addresses. The service network will be used to
      # machine the service nodes. This machine has two public IP addresses, one IPv4
      # and one IPv6.
      externalAddresses = [ "5.6.7.8" "2048:1337:1111::1" ];
      internalAddress = "10.0.0.2";
      serviceNetwork = "10.0.2.0/24";

      # Define two storage pools, that services can use to store persistent data.
      resources.hot.storagePool = {
        mountpoint = "/srv/hot";
      };
      resources.cold.storagePool = {
        mountpoint = "/srv/cold";
      };

      # Specify our tunnel through adelia, used to reach the other nodes of the
      # infrastructure. However, unlike `makari`, this machine does not make all of
      # its traffic go through `adelia` as it has its own routable IP addresses,
      # so we just need to make it route through other nodes of the infrastructure.
      resources.uplink.tunnelClient = {
        # Connect to the `gateway` resource on `adelia`
        server = "/adelia:gateway";
        # Re-use the machine's external addresses for the tunnel's interface
        # address
        addresses = externalAddresses;
        # Indicate that we want to route the traffic to other nodes through
        # the tunnel
        requires = [ "10.0.0.0/16" ];
        # Indicate that other machines connected to adelia may reach our service network.
        provides = [ serviceNetwork ];
      };

      # Specify a reverse proxy
      resources.proxy.reverseProxy = {
        # Make the reverse proxy listen both on the internal and public addresses
        addresses = externalAddresses ++ [ internalAddress ];
        # Make makari responsible for generating SSL certificates, so we avoid generating
        # certificates for each node in the infrastructure, which would be a problem with
        # the ACME provider's rate-limiting.
        certPrimary = "/makari:proxy";
      };

      # Specify the inner system configuration
      system.nixos.config = ./machines/kayou;
      platform = "x86-64_linux";
    };

    # In our example infrastructure, tablo is an external, unmanaged machine. It
    # provides a backup server (using Borg), that we will be used by some 
    # specific services.
    tablo = {

      # Set the machine's qualified name. This name should resolve to the machine's
      # public address.
      qualifiedName = "tablo.copains.org";

      # Specify that this system is externally managed : we do not generate its configuration.
      # However, this node may be used to gather data required for other nodes to connect to 
      # this machine. For example, since we provide a borg backup server, this node can be used
      # to collect the SSH usernames and public keys of each backup client, so that they can
      # be forwarded to our friends operating this machine.
      system.unmanaged = { };

      # Specify that this machine provides a borg backup server.
      resources.backupServer.borgServer = {};
    };
  };
}
