{ lib, ... }: {

  imports = [ ./machines.nix ./services.nix ];

  # Potentially more stuff, such as operators, secrets, metadata, whatever
  # ...

}
