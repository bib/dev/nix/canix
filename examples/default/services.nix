{ lib, ... }: with lib; {

  config.services = {

    # First, specify a service providing an automatically backed-up Garage cluster.
    core-obj = {
      # Assign this service to machines
      instances.makari.internalAddress = "10.0.1.240";
      instances.kayou.internalAddress = "10.0.2.240";

      # Make the instance of this server on makari backup the contents of the 
      # Garage store on the Borg backup server provided by `tablo`.
      instances.bibix.resources.garage-backup.backupTarget = {
        server = "/tablo:backupServer";
        source = ".:garage";
        schedule = [
          {
            interval = "2 hours";
            quantity = 12;
          }
          {
            interval = "1 day";
            quantity = 7;
          }
          {
            interval = "1 week";
            quantity = 4;
          }
          {
            interval = "1 month";
            quantity = 12;
          }
          {
            interval = "1 year";
            quantity = 2;
          }
        ];
      };

      # Specify two persistent volumes, from storage pools on the service's
      # parent machine.
      resources.garage-data.persistentVolume = {
        pool = "..:cold";
        mountpoint = "/srv/garage/data";
      };
      resources.garage-meta.persistentVolume = {
        pool = "..:hot";
        mountpoint = "/srv/garage/meta";
      };

      # Specify a Garage cluster member
      resources.garage.garageStore = {
        port = 5487;
        peers = [ "/*/core-obj:garage" ];
      };

      # Specify the inner system configuration
      system.nixos.config = ./services/core-obj;

      # Override `nixpkgs` for this system with the given flake input.
      # This flake input should be defined in the infrastructure flake.
      inputs.nixpkgs = "nixpkgs_2305";

    };

    # Then, specify a service providing SQL databases, using MariaDB and PostgreSQL
    core-sql = {
      instances.makari.internalAddress = "10.0.1.34";
      instances.kayou.internalAddress = "10.0.2.34";

      # Use persistent volumes to store the SQL databases. Although this volume
      # is persistent, it is "less critical" as the databases will be synchronized to
      # the Garage store at regular intervals.
      resources.mysql-data.persistentVolume = {
        pool = "..:hot";
        mountpoint = "/srv/mysql";
      };
      resources.pgsql-data.persistentVolume = {
        pool = "..:hot";
        mountpoint = "/srv/pgsql";
      };

      # Define resources for the actual SQL servers
      resources.mysql-server.mysqlServer = {
        volume = ".:mysql-data";
        port = 1054;
      };
      resources.pgsql-server.pgsqlServer = {
        volume = ".:pgsql-data";
        port = 1055;
      };

      system.nixos.config = ./services/core-sql;

    };

    # Now, specify an application service (here, Nextcloud), that stores its data
    # in a database and local volume that are both synchronized with the Garage store.
    cloud = {

      # This service is only assigned to `makari`. However, it can be easily moved to `kayou`.
      instances.makari.internalAddress = "10.13.208.77";

      # Use a persistent volume for the Nextcloud cache. This volume is not critical.
      resources.cache.persistentVolume = {
        pool = "..:hot";
        mountpoint = "/srv/nextcloud/cache";
      };

      # Specify a Garage-backed local volume for Nextcloud's data.
      resources.files.storeVolume = {
        store = "../core-obj:store";
        pool = "..:cold";
        mountpoint = "/srv/nextcloud/data";
      };

      # Specify a Garage-backed MySQL database
      resources.database.mysqlDatabase = {
        database = "nextcloud";
        server = "../core-sql:mysql-server";
        store = "../core-obj:store";
      };

      # Expose ourselves through the reverse proxy on our parent machine.
      resources.backend.httpTarget = {
        domains = [ "cloud.example.org" ];
      };

      # Specify the inner system configuration
      system.nixos.config = ./services/cloud;
    };

    # Specify another application service (Forgejo), that also stores its data in
    # a database and local volume synchronized with the Garage store.
    forge = {
      instances.kayou.internalAddress = "10.13.203.61";

      # Specify a Garage-backed local volume for Forgejo's repositories & data.
      resources.files.storeVolume = {
        store = "../core-obj:store";
        pool = "..:cold";
        mountpoint = "/srv/forgejo/data";
      };

      # Specify a Garage-backed MySQL database
      resources.database.mysqlDatabase = {
        database = "forge";
        store = "../core-obj:store";
        server = "../core-sql:mysql-server";
      };

      # Expose ourselves through the reverse proxy on our parent machine.
      resources.proxy.httpTarget = {
        domains = [ "forge.example.org" ];
      };

      # Specify the inner system configuration
      system.nixos.config = ./services/forge;

    };

    # Specify a very simple application container that exposes static websites
    # machineed in the Garage store.
    sites = {
      # Assign this service to both makari and kayou.
      instances.makari.internalAddress = "10.0.1.78";
      instances.kayou.internalAddress = "10.0.2.78";

      # Specify the bucket that provides the static sites
      resources.sites-data.storeBucket = {
        store = "..:store";
        bucket = "sites";
      };

      # Exposes ourselves through the reverse proxy on our parent machine.
      resources.proxy.httpTarget = {
        domains = [
          "weshlesvoyous.com"
          "scandalesanitaire.fr"
          "derivepostmoderniste.lgbt"
        ];
      };

      # Specify the inner system configuration
      system.nixos.config = ./services/sites;
    };
  };
}
