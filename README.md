# Canix — static infrastructure deployment

## Introduction

Canix is an opinionated declarative inrastructure management tool written in Nix, intended for small-scale infrastructures with a few machines that each run isolated services, using containers or virtual machines. 

Canix is built, like NixOS, with the **Nixpkgs module system**, which provides a configuration language that provides type-checking, documentation, and extensibility. While NixOS uses this module system to describe the configuration of a single operating system, Canix uses the module system to describe infrastructure, by specifying its host nodes ("machines" and "services") and how they interact together (through "resources"). This infrastructure specification can then be used to build the nodes inner configurations.

This may a be a bit too abstract, so let's consider a concrete example : say we use NixOS to configure a set of machines, that each host services such as web applications, a mail infrastructure, an SQL server, Garage cluster, exposed through a reverse proxy, and let's assume that the components of a service on a given machine may need to access services on other machines. NixOS provides a very good solution to declaratively manage the configuration of these machines and services, however the "relations" between these nodes will have to be managed manually in the nodes configuration, and the operators will have to maintain consistency themselves.

For example, in the configuration of our web applications, we will need to refer to the SQL server, which may be on another host, same for the SMTP server, reverse proxy, and other components. This not only means that we have to specify, in the configuration of each service, the address of the other components it requires, but that we also need to manage the secrets and keep them in sync. 

As the infrastructure's configuration gets larger, these relations get harder and harder to manage, especially as they interact with the deployment process, and maintaining the coherence of the infrastructure gets more difficult. This makes it more difficult to make understand or make changes to the infrastructure, and often implies that secrets are never rotated.

Canix provides a language to represent the relations between these nodes, using predefined "resource" types, so as to form a "specification" of the infrastructure, that can be referenced when generating inner system configurations so that they can be written with less coupling to the rest of the infrastructure.

On top of that, Canix provides some tools to apply this specification to the infrastructure's nodes, and to generate and deploy the secrets referenced by the specification. This can be used to manage an infrastructure with a collective process, for contexts where we want to avoid anyone having privileged access to machines that host user data.

Canix also leverages Nix and NixOS to make containers share their store with their parent host, and to allow secrets being loaded (from a trusted host) in secure memory on machine startup.

Canix is currently written as a Nix library that provides a way to transform an input specification into a set of node configurations (for example, NixOS configurations), and a set of deployment tools that can be used to remotely apply the evaluated configuration to the infrastructure's machines. This library is used to create "canix infrastructure flakes", such as the ones that can be seen in `./examples/*`.

## Implemented resource types

 - IP backbone tunnels are used to set-up point-to-point Wireguard links between machines, and can be used to route IP addresses through a tunnel to machines that are not in an IP space we control.
 - Reverse proxying (using haproxy) frontends/backends
 - MySQL/PostgreSQL database resources (optionally synced to/from Garage buckets)
 - Persistent filesystems (optionally synced to/from Garage buckets)
 - Garage S3 buckets
 - Arbitrary secrets
