{ lib, canix, ... }:
with lib; 

{

  isolationTypes = {
    ociContainer = mkOption {
      description = ''
        Use a Linux container (through `runc`) to isolate the service from its machine and 
        other services.
      '';
      type = canix.submodule {
        options = {
          # TODO: add container runtime configuration options
        };
      };
    };
    ociVirtualMachine = mkOption {
      description = ''
        Use a Linux kernel virtual machine (through `crun`) to isolate the service from
        its machine and other services.
      '';
      type = canix.submodule {
        options = {
          # TODO: add virtual machine runtime configuration options
        };
      };
    };
  };

}
