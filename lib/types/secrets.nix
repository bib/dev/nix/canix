{ lib, ... }:
with lib;
let

  # TODO
  secretType = types.submodule {
    options = {
      path = mkOption {
        description = ''
          The path where the generated secret should be stored, in the secret
          storage repository.
        '';
        type = types.nonEmptyStr;
      };
      script = mkOption {
        description = ''
          A script that runs from the root of the secret storage repository, which
          should output the generated secret.
        '';
        type = types.nonEmptyStr;
      };
    };
  };

in {

  # TODO
  mkPasswordOption = config: mkOption ({
    type = secretType;
    default.path = "XXX";
    default.script = ''
      pwgen -s 32 1
    '';
  } // config);

  # TODO
  mkSecretOption = attrs: mkOption ({ type = secretType; } // attrs);

}
