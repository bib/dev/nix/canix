{ lib, canix, ... }:
with lib;
let

  /* * Make a type that bundles a node path with typing
      information about the target node.
  */

  checkNode = path: node: nodeType: resourceType:
    if node == null then throw ''
      No nound found at path '${path}' (${merged}).
    '' else if node._nodeType != nodeType then throw ''
      Expected node type `${nodeType}` at ${path}, got `${node._nodeType}`.
    '' else if node._resourceType != resourceType then throw ''
      Expected resource type `${resourceType}` at ${path}, got `${node._resourceType}`.
    '' else node;

  nodePathOptionType = context: nodeType: resourceType: mkOptionType {
    name = if nodeType == canix.RESOURCE
           then "resource(${resourceType})-path"
           else "${nodeType}-path";
    description = if nodeType == canix.RESOURCE
                  then "path to a resource of type ${resourceType}"
                  else "path to a ${nodeType}";
    descriptionClass = "noun";
    merge = loc: defs: let 
      merged = mergeEqualOption loc defs;
      resolved = canix.resolveNodePath context merged;
      node = canix.getNode context resolved;
    # FIXME: this is not the good place to do these this checks
    #        however we cannot do it in "checks" because it applies 
    #        to the initial definition, and not to the merged 
    #        (resolved) value. Knowing where to do this is relatively 
    #        tricky, as the failure of this checks will probably make
    #        the evaluation fail elsewhere. One possible solution would
    #        be to automatically generate assertions for all path types,
    #        but it doesnt't feel really right.
    in checkNode resolved node nodeType resourceType;
  };

  mkPathOption = context: nodeType: resourceType: attrs: 
    mkOption ({
      type = nodePathOptionType context nodeType resourceType;
    } // attrs);

in {

  # * Make a path option for nodes of any type.
  mkNodePathOption = context: mkPathOption context null null;
  mkNodePathsOption = context: mkPathsOption context null null;

  # * Make a path option for  machine nodes.
  mkMachinePathOption = context: mkPathOption context "machine" null;
  mkMachinePathsOption = context: mkPathsOption context "machine" null;

  # * Make a path option for service nodes.
  mkServicePathOption = context: mkPathOption context "service" null;
  mkServicePathsOption = context: mkPathsOption context "service" null;

  # * Make a path option for resource nodes.
  mkResourcePathOption = context: mkPathOption context "resource";
  mkResourcePathsOption = context: mkPathsOption context "resource";

}
