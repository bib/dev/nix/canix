{lib, ...}: with lib; rec {

  assertionType = types.submodule { 
    options = {
        
      assertion = mkOption {
        description = "The expression expected to be true.";
        type = types.bool;
      };
        
      message = mkOption {
        description = "The message to show when the expression is false.";
        type = types.str;
      };

    };
  };

  assertionsOption = mkOption {
    description = ''
      A set of static assertions that should all evaluate to true for the 
      evaluation to be successful.
    '';
    visible = "shallow";
    type = types.listOf assertionType;
    example = [ { assertion = false; message = "this is unsupported because ..."; } ];
    default = [];
  };

}