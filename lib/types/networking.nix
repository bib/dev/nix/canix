{ lib, canix, ... }:
with lib;
let

  rateLimitType = canix.submodule {
    options = {
      limit = mkOption {
        description = ''
          Maximum amount of bytes that can be queued waiting for tokens.

          Should not be too big to avoid adding too much latency, but making
          it big enough so that packets are not dropped too early.
        '';
        type = types.singleLineStr;
        example = "500 kbit";
      };
      rate = mkOption {
        description = ''
          The speed at which the bucket should be filled with tokens.

          Should be set to the administrative rate limit.
        '';
        type = types.singleLineStr;
        example = "100 mbit";
      };
      burst = mkOption {
        description = ''
          Size of the rate limiter bucket, in bytes. 

          Should bet set to a minimum of `rate / HZ` to avoid token starvation, 
        '';
        type = types.singleLineStr;
        defaultText = "rate / 100";
        example = "1 mbit";
      };
    };
  };

in {
  limitsOptionType = canix.submodule {
    options = {
      up = mkOption {
        description = ''
          Upstream rate-limits (named arguments passed to `tc-tbf`).
        '';
        type = types.nullOr rateLimitType;
        default = null;
      };
      down = mkOption {
        description = ''
          Upstream rate-limits (named arguments passed to `tc-tbf`).
        '';
        type = types.nullOr rateLimitType;
        default = null;
      };
    };
  };
}
