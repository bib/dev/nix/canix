{ nixpkgs, ... }:
with nixpkgs.lib;
let

  # We load `pkgs` and `lib` from the `nixpkgs` flake input.

  # FIXME: need better handling for nixpkgs.system (to be fixed in flake.nix)
  pkgs = (import nixpkgs { system = "x86_64-linux"; });
  lib = nixpkgs.lib;

  # We use a recursive definition for `canix`, so that each module can use attributes
  # from other modules (as long as there is no cyclic definition, of course).
  canix = let loadModule = path: (import path) { inherit canix lib pkgs; };
  in lib.attrsets.mergeAttrsList (map loadModule [
    ./types/default.nix
    ./types/networking.nix
    ./types/secrets.nix
    ./types/assertions.nix
    ./types/paths.nix
    ./utils/default.nix
    ./utils/paths.nix
    ./utils/submodules.nix
  ]);

  # Evaluate the given infrastructure modules
  evalSpecModules = modules: lib.evalModules {
    class = "canix";
    specialArgs = { inherit canix; };
    modules = (toList modules) ++ [
      # Context module (used to provide named scopes)
      (canix.makeContextModule { scopeName = "spec"; })
      # Specification options
      ./../modules/default.nix
      # Default set of resource types provided by Canix.
      ./../resources/default.nix
      # Default set of system types provided by Canix.
      ./../systems/default.nix
    ];
  };

in {

  /** TODO

  */
  evalSpec = modules: lib.pipe modules [
    (modules: (evalSpecModules modules).config)
    canix.checkAssertions
    canix.filterSpec
  ];

  /* TODO

  **/
  specOptions = removeAttrs (evalSpecModules []).options [ "_module" ];

}
