/** 

   TODO: Add file docstring

*/
{ lib, canix, ... }: 
with lib; 

rec {

  # Enumeration types
  # =====================================================================

  MACHINE = "machine";
  SERVICE = "service";
  RESOURCE = "resource";

  # Trivial utilities
  # ==========================================================================

  /** Check whether an attribute exists and is null

      Type: isAttrNull ⸪ String → AttrSet → AttrSet`
  
  */
  isAttrNull = name: attrs: isNull (attrs."${name}" or null);


  /** Map a function over a list, then flatten the result.
  
      Type: fmap ⸪ (T → [U]) → [T] → [U]

  */
  fmap = f: xx: flatten (map f xx);

  /** Extend `scopes` with `name` set to `config`, when `name` is not null

      Type: extendScopes ⸪ AttrSet → str → AttrSet

  */
  extendScopes = scopes: name: config: 
    if isNull name then scopes else setAttr scopes name config;

  /** Recursively filter the specification to remove any attribute whose name
      starts with "_".

      Type : filterSpec ⸪ AttrSet → AttrSet

  */
  filterSpec = spec:
    let keepAttr = name: value: ! (hasPrefix "_" name);
    in filterAttrsRecursive keepAttr spec;


  /** Get the tag name of a tagged type. In nixpkgs's standard library,
  a tagged attribute is represented as an attribute set where the
  tag name is the first and only name in the attribute set.

  */
  tagName = attrs: head (attrNames attrs);
  tagNames = attrs: mapAttrs (_: tagName) attrs;

  /** Get the tag value of a tagged type. In nixpkgs's standard library,
  a tagged attribute is represented as an attribute set where the
  tagged value is the first and only value in the attribute set.

  */
  tagValue = attrs: head (attrValues attrs);
  tagValues = attrs: mapAttrs (_: tagValue) attrs;

  # Scoped submodules
  # ==========================================================================

  /** Build a special module that injects the context argument for scoped
      submodules.

      Notes:
       - `label` is generally used to define variant types (such as the ones
         in `environment`).
       - `scopes` is used in Canix's node types, for example when resources
         need to know about the parent  machine.

  */
  baseContext = { scopes = {}; self = null; };
  makeContextModule = {
    context ? baseContext,
    label ? null,
    scopeName ? null,
  }: { ... } @ scope: let 
    context'.scopes = extendScopes context.scopes scopeName scope;
    context'.label = if isNull label then scopeName else label;
    context'.scopeName = scopeName;
    context'.self = scope.config;
    context'.parent = context.self;
  in { 
    config._module.args.context = context // context';    
  };


  /** Make a scoped submodule type, by wrapping nixpkgs's `submodule`,
      extending so that the submodules receive a `context` argument, 
      that allow them to get access to parent scopes
      (through `context.scopes`) and to know the "label" of the object
      being defined (through `context.label`).

      Arguments :
       - `modules` : the list of modules to use for the submodule's type
       - `args` : the arguments passed to build the context
                  (see `makeContextModule` for more information)

  */
  scopedSubmodule = modules: args: canix.submodule (
    (toList modules) ++ [ (makeContextModule args) ]
  );

  # Node traversal function
  # ==========================================================================

  /** This function traverses the machine mapping, and collects the result of
      calling the given function into the resulting list.
  
      Type: 
        node ⸪ Machine | Service | Resource
        fmapSpec ⸪ (node → T) → { Machine } → [ T ]
        
   */
  fmapSpec = func: machines:
    let
      fmapResources = rs: fmap func (attrValues (tagValues rs));
      fmapNode = h: (fmapResources h.resources) ++ (func h);
      fmapServices = ss: fmap fmapNode (attrValues ss);
      fmapMachine = h: (fmapServices h.services) ++ (fmapNode h);
    in fmap fmapMachine (attrValues machines);

  # Assertions checking
  # ==========================================================================

  checkAssertions = spec: let
    sources = spec.nodeMapping // spec.services;
    failed = flatten (mapAttrsToList (path: node: map (assertion:
      " - in ${path} : ${assertion.message}"
    ) (filter (x: !x.assertion) node.assertions)) sources);
  in throwIf ((length failed) > 0) ''
    Failed to evaluate the specifications because of failed assertions :
    ${concatStringsSep "\n" failed}
  '' spec;

  # Resource and system types
  # ==========================================================================

  /** Internal function to derive an attribute tag type for an
      extensible type that is configured through the specification's
      evaluation environment.

  */        
  mkTaggedType = {context, tagTypes, baseModule, scopeName}: let
    # Filter type descriptions to have only the ones 
    # compatible with the current node's type.
    nodeType = if context?service then canix.SERVICE else canix. MACHINE;
    checkType = type: type.parentTypes.${nodeType};
    filteredTypes = filterAttrs (_: checkType) tagTypes;
    # For a given type description, make the tag's option.
    # See `types.attrTag` for more details.
    makeTag = name: type: let 
      freeformModule.freeformType = types.unspecified;
      modules = if length type.modules == 0 
                then [ freeformModule baseModule ]
                else [ baseModule ] ++ type.modules;
    in mkOption {
      inherit (type) description;
      type = canix.scopedSubmodule modules { inherit context scopeName; };
    };
    # Map the filtered types and define the final tagged attribute type
  in types.attrTag (mapAttrs makeTag filteredTypes);

  /** Create the attribute tag type used to represent node's resources,
      using `mkTaggedType`. The type will only be able to represent
      resources compatible with the current node type. */
  mkResourceType = context: mkTaggedType {
    inherit context;
    tagTypes = context.scopes.spec.config.environment.resourceTypes;
    baseModule = ../modules/resource.nix;
    scopeName = "resource";
  };

  /** Create the attribute tag type used to represent node's resources,
      using `mkTaggedType`. The type will only be able to represent
      systems compatible with the current node type. */
  mkSystemType = context: mkTaggedType {
    inherit context;
    tagTypes = context.scopes.spec.config.environment.systemTypes;
    baseModule = ../modules/system.nix;
    scopeName = "system";
  };


}