{ lib, ... }:

with lib;
with lib.strings;

let

  # Basic path resolution and expansion functions
  # ==========================================================================

  notEmpty = string: stringLength string > 0;
  splitPath = path: filter notEmpty (splitString "/" path);
  joinPath = parts: concatStringsSep "/" ([""] ++ parts);
  tryInit = x: if length x == 0 then x else init x;
  resolvePath' = x: y: if length y == 0 then
    x
  else if head y == "." then
    resolvePath' x (tail y)
  else if head y == ".." then
    resolvePath' (tryInit x) (tail y)
  else
    resolvePath' (x ++ [ (head y) ]) (tail y);

  resolvePath = current: path: 
    if hasPrefix "/" path then path else
    joinPath (resolvePath' (splitPath current) (splitPath path));

  globToRegex = glob:
    let escaped = escape [ "\\" "^" "$" "." "+" "|" "(" ")" "[" "{" "}" ] glob;
    in replaceStrings [ "\\*" "*" "\\?" "?" ] [ "*" ".*" "\\?" ".?" ] escaped;

  matchPaths = paths: globs:
    let
      patterns = map globToRegex globs;
      testPattern = pattern: path: (match pattern path) != null;
      testPath = path: any (map testPattern patterns);
    in filter testPath paths;

  applyToPath = f: path:
    if ! (hasInfix ":" path) then f path else let
      parts = splitString ":" path;
    in concatStringsSep ":" [ (f (head parts)) (last parts) ];

in {

  resolveNodePath = context: let 
      currentNode = context.scopes.service or context.scopes.machine;  
  in applyToPath (resolvePath currentNode.config.path);

  getNode = context: path: 
    let 
      currentNode = context.scopes.service or context.scopes.machine;
      resolved = applyToPath (resolvePath currentNode.config.path) path;
    in context.scopes.spec.config.nodeMapping.${resolved} or lib.fnull;

  findNode = context: patterns:
    let 
      resolved = map (path: applyToPath (resolvePath context.self.path) path) paths;
      patterns = map globToRegex paths;
      testPattern = pattern: path: node: 
        if (match pattern path) != null then node else null;
      # TODO: not finished
      testPath = path: null;       
      filterPaths = matchPaths context;
      expandPath = path;
  in flatten (map expandPath paths);

}