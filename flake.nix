{
  description =
    "Static and declarative infrastructure management framework written in the Nix language";

  /** Inputs
      ========================================================================
  
      We base ourselves on NixOS 24.05, however this is only used to fetch
      Nix's standard library (which is defined in `nixpkgs` itself) and to 
      generate deployment tools.

      Although we expose a "flake module" in the sense defined by
      `flake-parts`, this flake module is only used in infrastructure flakes 
      themselves, and we don't need a dependency to flake-parts here, just to 
      respect its conventions.

  */
  inputs = {
    # We depend on nixpkgs solely for its standard library and
    # for the building of deployment tools.
    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.05";
  };

  /** Outputs
      ======================================================================

      We provide a "flake module", in the sense defined by 
      [flake-parts](https://flake.parts), that can be used by infrastructure
      flakes to automatically define their outputs (processed specification,
      NixOS configurations, deployment assets & shell, documentation, etc)
      based on the specification confiured in the "canix" namespace.

      This provides a somewhat standardized scheme to compose infrastructure
      from multiple flakes, for example to extend Canix with additional 
      options, or with additional resource types.

      Specifications themselves are "system-independent" : although their 
      nodes will be defined for a specific system, the specification itself
      is "plain-old data", and should not contain any reference to the Nix 
      store.

    */
  outputs = { nixpkgs, flake-parts, ... } @ inputs: let 

    # The systems on which we can build the 
    # documentation and development tools.
    # TODO: test and add other architectures
    systems = [ "x86_64-linux" "x86-linux" ];

    # Utility function to generate our outputs for each compatible system.
    forEachSystem = contents: builtins.listToAttrs (map contents systems);

  in {

    # The examples, that can be used as flake templates
    templates = import ./examples;

    # Flake module
    flakeModule = ./flake-module.nix
    
   } // (forEachSystem (system: let 
 ?>>>>>>>>
      # Import `nixpkgs` for the current system.
      pkgs = import nixpkgs { inherit system; };

    in {

      # TODO: add documentation, schema, and Canix devshell

    }));
  };
}
