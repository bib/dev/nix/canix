# `lib/modules`

This directory contain the NMS modules used to implement Canix. These submodules are
implemented using `canix.submodule` rather than nixpkgs's `types.submodule` so that 
they inherit the `context` argument that provides them access to the parent submodules, 
as an ad-hoc form of "lexical" scoping. 

For example, in `config.machines.myMachine.services.myService`, `context` will provide access
to the configuration of the parent machine (`config.machines.myMachine`) and to the 
specification itself (`config`).

**NOTE:** All files in this directory should be regular NMS modules, in addition to the
          usual `lib`, `config` and `options` arguments, they can all expect a `context`
          argument (described above) and a `canix` argument, which provides access to 
          the Canix library itself. See `lib/utils/submodules.nix` for more information.

## Extendable resource and system types

The allowed types for node's `resources` and `system` options are defined in
`config.environment.resourceTypes` and `config.environment.systemTypes` so that
they can be easily extended. The values of these options are then used to build the 
type for the `resources` and `system` options of machine and service nodes.
