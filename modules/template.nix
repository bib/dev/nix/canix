{ lib, config, context, ... }:
with lib; {

  freeformType = types.unspecified;

  options = {

    instances = mkOption {
      description = ''
        The machines on which this service template should be enabled, along
        with the configuration specific to the instantiation of this service on
        the machine.
      '';
      type = types.attrsOf (types.unspecified);
      visible = "shallow";
      default = {
        my-host-1.internalAddress = "10.0.1.1";
        my-host-2.internalAddress = "10.0.1.2";
      };

    };

  };

  config = {

    assertions = let 
      machineNames = attrNames context.scopes.spec.config.machines;
    in map (machineName: {
      assertion = elem machineName machineNames;
      message = "instantiating service on undefined machine `${machineName}`";
    }) (attrNames config.instances);

  };

}
