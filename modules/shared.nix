{ lib, canix, context, ... }: with lib; {
  
  options = {

    internalAddress = mkOption {
      description = ''
        The internal IP address of a ${context.label}. This address is used when the
        resources of other nodes want to address this machine and its 
        resources.
      '';
      type = types.nullOr types.singleLineStr;
      default = null;
      example = "10.0.0.1";
    };

    inputs = mkOption {
      description = ''
        A mapping of flake input names to other input names, used to
        override specific inputs when evaluating the inner system
        configuration of this ${context.label}.
      '';
      type = types.attrsOf types.singleLineStr;
      default = { };
      example = { nixpkgs = "nixpgs_2305"; };
    };

    resources = mkOption {
      description = ''
        The resources consumed and provided by this ${context.label}.

        Note that some resource types are only available on machines, 
        and not their services.
      '';
      type = types.attrsOf (canix.mkResourceType context);
      default = { };
    };

    system = mkOption {
      description = ''
        The inner system configurations of this service.
      '';
      #type = types.attrTag (canix.mkSystemType context);
      default = { };
    };

    platform = mkOption {
      description = ''
        The machine's platform (equivalent to the `system` attribute in \n	Nix flake outputs).\n
      '';
      type = types.nullOr types.str;
      default = null;
      example = "x86_64-linux";
    };

  };

}