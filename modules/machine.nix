{ lib, canix, config, context, path, ... }:
with lib; {

  imports = [
    ./shared.nix
  ];

  options = {

    name = mkOption {
      description = ''
        TODO
      '';
      type = types.singleLineStr;
      default = last path;
    };

    path = mkOption {
      description = ''
        TODO
      '';
      type = types.singleLineStr;
      default = "/${config.name}";
      readOnly = true;
    };

    _nodeType = mkOption {
      default = canix.MACHINE;
      internal = true;
      visible = false;
    };

    assertions = canix.assertionsOption;

    qualifiedName = mkOption {
      description = ''
        The fully-qualified name of this machine. This name should always
        resolve to one of the machine's external IP addresses
      '';
      type = types.singleLineStr;
      example = "foo.example.org";
    };

    externalAddresses = mkOption {
      description = ''
        The external IP addresses of this machine.
      '';
      type = types.listOf types.singleLineStr;
      default = [ ];
      example = [ "192.0.2.1" "198.51.100.1" ];
    };

    # TODO: Add assertion requiring this setting for machines with services.
    serviceNetwork = mkOption {
      description = ''
        The internal network used for services.

        This setting is only required for machines that host services.
      '';
      type = types.nullOr types.singleLineStr;
      default = null;
      example = "10.0.1.0/24";
    };

    services = mkOption {
      description = ''
        The service instantiated on this machine.
      '';
      type = types.attrsOf (canix.scopedSubmodule ./service.nix {
        inherit context;
        scopeName = "service";
      });
      default = { };
      #visible = "shallow";
    };
  };

  # Machine config definitions
  # ==========================================================================

  config = {

    services = let
      makeService = service: mkMerge [ 
        (removeAttrs service [ "instances" ])
        (service.instances.${config.name})
      ];
      hasInstance = service: service.instances?${config.name};
      filterDef = def: filterAttrs (_: hasInstance) def;
      makeServices = def: mapAttrs (_: makeService) (filterDef def);
      serviceDefs = context.scopes.spec.options.services.definitions;
    in mergeAttrsList (map makeServices serviceDefs);

  };

}
