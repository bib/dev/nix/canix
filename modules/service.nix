{ lib, canix, config, path, context, ... }:
with lib; {

  imports = [
    ./shared.nix
  ];

  options = {

    name = mkOption {
      description = ''
        TODO
      '';
      type = types.singleLineStr;
      default = last path;
    };

    path = mkOption {
      description = ''
        TODO
      '';
      type = types.singleLineStr;
      default = "${config.parent}/${config.name}";
      readOnly = true;
    };

    parent = mkOption {
      description = ''
        TODO
      '';
      type = types.singleLineStr;
      default = context.parent.path;
      readOnly = true;
    };

    _nodeType = mkOption {
      default = canix.SERVICE;
      internal = true;
      visible = false;
    };
    
    assertions = canix.assertionsOption;

    isolation = mkOption {
      description = ''
        The service's isolation mechanism (container, virtual machine).
      '';
      type = types.attrTag canix.isolationTypes;
      default = { ociContainer = { }; };
    };

  };

  config = {

  };

}
