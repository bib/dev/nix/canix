{ config, lib, pkgs, spec, canix }:
let

  # This function builds the systemd units (and parts of the BIRD configuration)
  # required for setting up a backbone wireguard tunnel. 
  makeTunnel = server: client: {
    systemdNetdev = {

      # 1. Tunnel configuration (virtual layer 2)
      # ----------------------------------------------------------------------

      # We define a new virtual network device implemented using Wireguard
      netdevConfig.Kind = "wireguard";
      netdevConfig.Name = name;
      # We configure all settings not speciic to a single peer
      wireguardConfig.PrivateKeyFile = expandSecret server.privateKey;
      wireguardConfig.ListenPort = server.listenPort;
      wireguardConfig.MTU = client.mtu;
      # We configure all settings relating to the remote peer
      wireguardPeers = [{
        wireguardPeerConfig = {
          AllowedIPs = server.networks ++ [ client.address ];
          PublicKey = expandSecret client.publicKey;
          PresharedKeyFile = expandSecret client.preSharedKey;
          PersistentKeepalive = 5;
        };
      }];
    };

    systemdNetwork = {

      # 2. Network configuration (layer 3)
      # ----------------------------------------------------------------------

      # We define a new systemd network configuration on the tunnel's interface
      inherit name;
      linkConfig.RequiredForOnline = false;
      # We configure a point-to-point IP address to assign to this interface
      addresses = [{
        addressConfig = {
          Address = server.address;
          Peer = client.address;
        };
      }];

      # 3. Rate limiting configuration
      # ----------------------------------------------------------------------

      # Apply shaping to the root qdisc, because we want to apply it to all egress packets
      tokenBucketFilterConfig.Parent = "root";
      tokenBucketFilterConfig.Rate = client.limits.down.rate or null;
      tokenBucketFilterConfig.BurstBytes = client.limits.down.burst or null;
      tokenBucketFilterConfig.PeakRate = client.limits.down.peak or null;
      tokenBucketFilterConfig.MTU = client.mtu;
    };

    # 2. Network configuration (layer 3)
    # ----------------------------------------------------------------------

    ospfInterface = {
      # Tell OSPF that this is a point-to-point interface
      type = "ptp";
      # Enable BFD link monitoring
      bfd = true;
      # Avoid sending messages larger than the tunnel's MTU
      tx_length = client.mtu;
    };
  };

  # We prepare a list of tunnel definitions, one for each consumer of each tunnel
  tunnels = let
    servers = listResources spec.node TUNNEL_SERVER;
    makeClientTunnels = server: map (makeTunnel server) server.clients;
  in flatMap makeClientTunnels servers;

in {
  # Map tunnel definitions to systemd network units
  systemd.network = mkMerge {
    netdevs = map (tunnel: tunnel.systemdNetdev) tunnels;
    networks = map (tunnel: tunnel.systemdNetwork) tunnels;
  };

  # Map tunnel definitions to BIRD2 OSPF interface definitions
  services.canix-bird2.ospf.areas."0.0.0.0" =
    mkMerge { interfaces = map (tunnel: tunnel.ospfInterface) tunnels; };
}
