{ ... }: {

  imports = [
    ./mysqlDatabase
    ./mysqlServer
  ];

  config = {
    environment.resourceTypes = {
      tunnelServer = {};
      tunnelClient = {};
      persistentVolume = {};
      garageStore = {};
      backupTarget = {};
      pgsqlServer = {};
      storeVolume = {};
      httpTarget = {};
      storeBucket = {};
      storagePool = {};
      reverseProxy = {};
      borgServer = {};
    };
  };

}
