{ lib, canix, ... }:
with lib; {

  # Resource metadata
  # ========================================================================

  description = ''
    An IP backbone tunnel, used to provide routable
    addresses to service hosts. This will set up a dedicated point-to-point
    for each uplink tunnel.
  '';
  nodes = with canix; [ MACHINE ];

  # Template options
  # ========================================================================

  template.options = {

    server = canix.mkResourcePathOption context "tunnelServer" {
      description = ''
        The tunnel server to connect to.
      '';
      example = "/machine1:gateway";
    };

    addresses = mkOption {
      description = ''
        The addresses to assign to the tunnel's interface.
      '';
      type = types.listOf types.singleLineStr;
      example = [ "192.0.2.240" "198.51.100.240" ];
    };

    cost = mkOption {
      description = ''
        The cost assigned to this link.
      '';
      type = types.numbers.nonnegative;
      default = 1000;
      example = 100;
    };

    limits = mkOption {
      description = ''
        The rate limiting to apply to this tunnel.
      '';
      type = canix.limitsOptionType;
      default = { };
    };

    requires = mkOption {
      description = ''
        The networks that we want to route to the tunnel's peer.
      '';
      type = types.listOf (types.singleLineStr);
    };

    provides = mkOption {
      description = ''
        The networks for which we expose routes to the tunnel's peer.
      '';
      type = types.listOf (types.singleLineStr);
    };
  };

  # Instance options
  # ==========================================================================

  instance.options = {

    endpoint = mkOption {
      description = ''
        The socket address that the tunnel should connect to.
      '';
      type = types.singleLineStr;
      internal = true;
      # FIXME: remove when this is properly defined by the configuration
      default = "";
      defaultText = "#TODO: write default text";
    };

    privateKey = canix.mkSecretOption {
      description = ''
        The private key of this tunnel client, generated using `wg genkey`.
      '';
      internal = true;
      # FIXME: remove when this is properly defined by the configuration
      default = {
        path = "XXX";
        script = "XXX";
      };
      defaultText = "#TODO: write default text";
    };

    publicKey = canix.mkSecretOption {
      description = ''
        The public key of this tunnel's server, derived from 
        the server's private key using `wg pubkey`.
      '';
      internal = true;
      # FIXME: remove when this is properly defined by the configuration
      default = {
        path = "XXX";
        script = "XXX";
      };
      defaultText = "#TODO: write default text";
    };

    preSharedKey = canix.mkSecretOption {
      description = ''
        The pre-shared key of this tunnel, generated using `wg genkey`.
      '';
      internal = true;
      # FIXME: remove when this is properly defined by the configuration
      default = {
        path = "XXX";
        script = "XXX";
      };
      defaultText = "#TODO: write default text";
    };

  };

  # Instance config
  # ==========================================================================

  instance.config = { };

  # System modules
  # ==========================================================================

  instance.system.nixos = ./nixos.nix;

}
