# `lib/resources`

This directory contains the default set of resources provided by Canix.

Each module is defined in `config.environment.resourceType` (see 
`lib/modules/resourceType.nix`), and indicates the submodules to use for 
the resource's options. The type for the `resources` option 
on machine and service nodes is then built by reading the resource types. 
See `lib/utils/resources.nix` for more information.
