{ lib, canix, context, ... }:
with lib; {

  options = {

    port = mkOption {
      description = ''
        The port on which this server should listen.
      '';
      type = types.port;
      default = 3306;
    };

    volume = canix.mkResourcePathOption context "persistentVolume" {
      description = ''
        The volume on which the database files should be stored.
      '';
    };

    databases = mkOption {
      description = ''
        The databases provided by this server to database resources.
      '';
      internal = true;
      type = types.listOf (canix.scopedSubmodule ./database.nix {
        inherit context;
        scopeName = "database";
      });
      # FIXME: remove when this is properly defined by the configuration
      default = [ ];
    };

  };
  
}