{ lib, canix, config, context, ... }:
with lib; {

  options = {

    database = mkOption {
      description = ''
        The name of the database.
      '';
      type = types.singleLineStr;
    };

    server = canix.mkResourcePathOption context "mysqlServer" {
      description = ''
        The path to the MySQL server resource for this database.
      '';
    };

    store = canix.mkResourcePathOption context "garageStore" {
      description = ''
        The path to the Garage object store resource used to save and
        restore this database.
      '';
    };

    storeInterval = mkOption {
      description = ''
        The interval at which the database should be synchronized with Garage.
      '';
      type = types.singleLineStr;
      default = "60 seconds";
    };

    initial = mkOption {
      description = ''
        The initial data that should be loaded into this database if it 
        doesn't exist neither in the database server nor in the object store.
      '';
      type = types.nullOr types.pathInStore;
      default = null;
      example = "./services/cloud/initial.sql";
    };

    username = mkOption {
      description = ''
        The username used to connect to the MySQL database server.
      '';
      type = types.singleLineStr;
      internal = true;
      # FIXME: remove when this is properly defined by the configuration
      defaultText = "\${config.name}";
    };

    password = canix.mkPasswordOption {
      description = ''
        The password used to connect to the MySQL database server.
      '';
      internal = true;
    };

    serverAddress = mkOption {
      description = ''
        The socket address used to connect to the database server.
      '';
      type = types.singleLineStr;
      # FIXME: remove when this is properly defined by the configuration
      # default = "";
      defaultText = "\${server.parent.internalAddress}:\${server.port}}";
    };

    storeAddress = mkOption {
      description = ''
        The socket address used to connect to the store.
      '';
      type = types.singleLineStr;
      internal = true;
      # FIXME: remove when this is properly defined by the configuration
      default = "";
      defaultText = "\${store.parent.internalAddress}:\${store.port}}";
    };

    storeKey = canix.mkPasswordOption {
      description = ''
        The API key used to connect to the store.
      '';
      internal = true;
      # FIXME: remove when this is properly defined by the configuration
      default = {
        path = "XXX";
        script = "XXX";
      };
    };

  };

  config = let
    server = canix.getNode context config.server;
    serverParent = canix.getNode context server.parent;
    # store = canix.getResource context store.path;
    # storeParent = canix.getParent context config.store.path;

  in {

    username = config.database;

    # Is this the good way to do this ? Should'nt we add an indirection level and allow
    # mysqlServer to set its own target address, defaulting to internalAddress ?
    #
    # serverAddress = "${serverParent.internalAddress}:${toString server.port}";
    serverAddress = "ACAB";

    # same as above
    # storeAddress = "${storeParent.internalAddress}:${store.port}";
  };


}
