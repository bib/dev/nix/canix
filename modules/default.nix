{ lib, config, canix, context, ... }: with lib; {

  options = {

    environment = mkOption {
      description = ''
        Configuration of the specification's evaluation environment.
      '';
      type = canix.scopedSubmodule ./environment {
        inherit context;
        scopeName = "environment";
      };
    };

    operators = mkOption {
      description = "Infrastructure management operators";
      type = types.attrs;
      default = { };
    };

    services = mkOption {
      description = ''
        The templates of the infrastructure's service nodes. This option can 
        be used to declare services that should exist on multiple machines, with
        some options being different on specific machine, using the `instances`
        sub option to define on which machines this service should be enabled.

        See `canix.machines.‹name›.services` for information about the allowed
        values. Note that the template's configuration will only be type-checked
        when it is instantiated on a machine.

      '';
      type = types.attrsOf (canix.scopedSubmodule ./template.nix {
        inherit context;
        scopeName = "service";
      });
      default = {};
      example = {
        instances.my-host1.internalAddress = "10.0.0.1";
        instances.my-host2.internalAddress = "10.0.0.2";
        system.nixos.config = ./configs/my-service;
      };
    };

    machines = mkOption {
      description = ''
        The infrastructure's machine nodes.
      '';
      type = types.attrsOf (canix.scopedSubmodule ./machine.nix {
        inherit context;
        scopeName = "machine";
      });
      default = {};
    };
    
    assertions = canix.assertionsOption;

    nodeMapping = mkOption {
      description = "A mapping of node paths to node configurations";
      internal = true;
      visible = false;
      readOnly = true;
    };

  };

  config = {

    nodeMapping = let
      makeItem = node: if node?path then [ (nameValuePair node.path node) ] else [];
    in listToAttrs (canix.fmapSpec makeItem config.machines);

  };


}