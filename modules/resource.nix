{ lib, config, canix, context, path, ... }:
with lib; {

  options = {

    name = mkOption {
      description = ''
        TODO
      '';
      type = types.singleLineStr;
      default = last (init path);
    };

    path = mkOption {
      description = ''
        TODO
      '';
      type = types.singleLineStr;
      default = "${config.parent}:${config.name}";
      readOnly = true;
    };

    parent = mkOption {
      description = ''
        TODO
      '';
      type = types.singleLineStr;
      default = context.parent.path;
      #readOnly = true;
    };

    _nodeType = mkOption {
      default = canix.RESOURCE;
      internal = true;
      visible = false;
    };

    _resourceType = mkOption {
      default = last path;
      internal = true;
      visible = false;
    };

    assertions = canix.assertionsOption;

  };

}
