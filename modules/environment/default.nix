{ lib, canix, context, ... }: with lib; {

  options = {

    resourceTypes = mkOption {
      description = ''
        TODO
      '';
      type = types.attrsOf (canix.scopedSubmodule ./type.nix {
        inherit context;
        scopeName = "resourceType";
        label = "resource type";
      });
      default = {};
    };

    systemTypes = mkOption {
      description = ''
        TODO
      '';
      type = types.attrsOf (canix.scopedSubmodule ./system.nix {
        inherit context;
        scopeName = "systemType";      
        label = "system type";
      });
      default = {};
    };

  };
}