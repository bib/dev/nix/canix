{ lib, path, context, ... }: with lib; {

  options = {

    name = mkOption {
      description = ''
        The name of this ${context.label}.
      '';
      type = types.singleLineStr;
      default = last path;
    };

    description = mkOption {
      description = ''
        The description of this ${context.label}.
      '';
      type = types.singleLineStr;
      default = "";
    };

    modules = mkOption {
      description = ''
        Path to the submodule representing this ${context.label}.
      '';
      type = types.listOf types.path;
      default = [];
    };

    parentTypes = mkOption {
      description = ''
        The types of nodes that can contain this ${context.label}.
      '';
      type = types.submodule {
        options = {

          machine = mkOption {
            description = ''
              Whether this ${context.label} should be available on machine nodes.
            '';
            type = types.bool;
            default = true;
          };

          service = mkOption {
            description = ''
              Whether this ${context.label} should be available on service nodes.
            '';
            type = types.bool;
            default = true;
          };
            
        };
      };
      default = {};
    };

  };

}