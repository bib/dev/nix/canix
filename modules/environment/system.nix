{ lib, ... }: with lib; {

  imports = [
    ./type.nix
  ];

  options = {

    resourceModules = mkOption {
      description = ''
        For each resource type, the system configuration modules that
        should be automatically imported in the inner system's configuration.
      '';
      type = types.attrsOf (types.path);
      default = [];
    };

  };

}